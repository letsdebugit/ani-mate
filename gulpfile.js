var gulp = require('gulp');
var clean = require('gulp-clean');
var rename = require('gulp-rename');
var dest = require('gulp').dest;
var multidest = require('gulp-multi-dest');
var print = require('gulp-print').default;
var minifyCSS = require('gulp-minifier');
var minifyJS = require('gulp-minify');

var settings = {
    paths: {
        src: 'src',
        dist: 'dist',
        examples: [
            'examples'
        ],
        libraries: [
            'node_modules/markdown/lib/markdown.js'
        ]
    }
};

function suffix(items, text) {
    if (typeof items == "string") {
        return items + text;
    }
    else {
        return items.map(function(item) {
            return item + text;
        });
    }
}

function prefix(items, text) {
    if (typeof items == "string") {
        return text + items;
    }
    else {
        return items.map(function (item) {
            return text + item;
        });
    }
}

gulp.task('clean-dist',
    function () {
        return gulp
            .src(settings.paths.dist, { read:false, allowEmpty: true })
            .pipe(clean({force: true}));
    });


gulp.task('clean-examples',
    function () {
        return gulp
            .src(suffix(settings.paths.examples, '/lib'), { read: false, allowEmpty: true })
            .pipe(clean({ force: true }));
    });


gulp.task('clean',
    gulp.series('clean-dist', 'clean-examples'));


gulp.task('dist-examples',
    function () {
        return gulp
            .src(suffix(settings.paths.src, '/*'))
            .pipe(multidest(suffix(settings.paths.examples, '/lib/')));
    });

gulp.task('dist-libraries',
    function () {
        return gulp
            .src(settings.paths.libraries, '/*')
            .pipe(multidest(suffix(settings.paths.examples, '/lib/')));
    });


gulp.task('dist-css', function () {
    return gulp
        .src(suffix(settings.paths.src, '/ani-mate.css'))
        .pipe(print())
        .pipe(minifyCSS({
            minify: true,
            minifyCSS: true
        }))
        .pipe(rename(function (path) {
            path.basename += ".min";
        }))
        .pipe(dest(suffix(settings.paths.dist, '/')));
});

gulp.task('dist-js', function () {
    return gulp
        .src(suffix(settings.paths.src, '/ani-mate.js'))
        .pipe(print())
        .pipe(minifyJS({
            noSource: true,
            ext: { min: '.min.js' }
        }))
        .pipe(dest(suffix(settings.paths.dist, '/')));
});

gulp.task('dist',
    gulp.series('dist-libraries', 'dist-examples', 'dist-css', 'dist-js'));


gulp.task('build',
    gulp.series('clean', 'dist'));


gulp.task('watch',
    gulp.series(
        'build',
        function() {
            gulp.watch(settings.paths.src + '/**/*', gulp.series('dist'));
        })
);

gulp.task('default', gulp.series('build'));
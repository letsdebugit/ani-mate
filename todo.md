# To do

* Write that readme

* Rendering using HTML5 Canvas rather than DOM, optionally

* Piskel-c to default frame converter as one of the examples

* Another flavour of JSON frame format:
  coordinates, much more compact for some animations such as moving pixels:

  (1,1,1) (2,3,1)
  (1,1,0) (2,3,0)

* Write frame generator implementing:

  - The Game of Life
    - initial setup random or picked by user by clicking
    - rules as usual, applied to frame in `onNext` event where pixels are decayed or born
    - detection of empty frame as game-end

* Implement a function to measure fps, running a simple loop of full and diff renderings
  at max speed

* Implement southpark mouths :) which can be overlayed on top of existing images.
  Then implement a dialog window with two guys talking.

* Frame transformation APIs, executed in a sequence, also via script in `TRANSFORM` section
  `shift`
  `crop`
  `flip`
  `rotate`
  `resize`

* `scroll()` API, which moves a window over a large frame, rather than playing frames.
  Parameters:
    x,        x shift in each step
    y,        y shift in each step
    steps,    number of scroll steps
    speed,    time interval between steps
    loop,     loop after reaching end of sequence
    reverse   reverse direction after reaching end of sequence

* Package and publish to npm

* Provide a version transpiled to ES5 with babel



var liveServer = require("live-server");

var params = {
    port: 8182,
    host: "0.0.0.0",
    root: "./examples",
    mount: [
        ['/', './examples']
    ],
    open: "/",
    logLevel: 1,
    middleware: [
        function (req, res, next) { next(); }
    ]
};
liveServer.start(params);
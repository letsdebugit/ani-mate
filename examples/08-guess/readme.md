# Guess the Picture Game

In this example we have used frame generator to implement
a simple `Guess the Picture` game. The application loads a hidden
image, then uncovers it block by block at fixed intervals.
We've achieved this by overlaying Ani-Mate player above
the image.

The frame has initially all pixels set to gray. Once we start playing,
generator function clears a random pixel of the frame at each turn.
Effectively, this reveals another tile of the underlying image.
Animation ends when all tiles have been revealed or when user presses
`stop` button. And so we have created a game in a couple lines of code,
how clever is that!

'use strict';

// Player controller
let player = {
    // Ani-Mate instance
    instance: null,
    // Indicates that the game has been played
    played: false,
    // Image element
    image: null,
    // Counter element
    counter: null,

    // Triggered when player is ready for action
    ready() {
        player.instance = this;
        player.image = document.querySelector('.image-container img');
        player.counter = document.querySelector('.controls-container .counter');
        player.instance.playFirst();
    },

    // Starts the game
    start() {
        player.instance.play();
        player.played = true;
    },

    // Stops the game
    stop() {
        player.instance.stop();
        player.instance.playLast();
        player.played = false;
        player.updateCounter();
    },

    // Updates the counter showing remaining tiles
    updateCounter(remaining) {
        player.image.className = 'visible';
        let text = '';
        if (player.played) {
            text = remaining == 0
                ? 'Game over'
                : (remaining == 1 ? 'One tile remaining' : `${remaining} tiles remaining`);
        }
        player.counter.innerText = text;
    },

    // Frame generator
    reveal(parameters) {

        let generator = {
            tiles: [],
            frame: null,
            revealedFrame: null,

            // Number of pixels in the frame
            getCount: () => parameters.columns * parameters.rows,

            // On start create a filled frame
            // and a list of pixels to reveal
            onStart: () => {
                generator.frame = generator.createFrame(1);
                generator.revealedFrame = generator.createFrame(0);
                generator.tiles = generator.frame.getPixels();
            },

            // On next frame reveal another tile
            onNext: () => generator.revealTile(),

            // When frame rendered, uncover the image if it was still hidden
            onRender: () => player.updateCounter(generator.tiles.length),

            // Returns the frame at specified index
            getFrame: index => generator.isLast() ? generator.revealedFrame : generator.frame,

            // Reveals another tile by clearing a random pixel in the frame
            revealTile: () => {
                let index = Math.floor(Math.random() * generator.tiles.length);
                let tile = generator.tiles[index];
                if (tile) {
                    generator.frame.setPixel(tile.x, tile.y, 0);
                    generator.tiles.splice(index, 1);
                }
            }
        };

        return generator;
    }

};


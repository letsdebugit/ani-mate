# Ani-Mate Player Examples

To demonstrate the mighty power of `Ani-Mate` we've created a number of examples and demos.
As you can see, there's a myriad ways you can use `Ani-Mate` for greater good or just for fun.
Use cases span from running simple animations all the way to actual interactive games. Enjoy!

// Player #1 Controller
let player1 = {

    instance: null,

    ready() {
        player1.instance = this;
    },

    getSpeed(index, count, isReverse) {
        return Math.max(10,
            Math.min(150, 1 + ((index * index * index) / 100)));
    }
};


// Player #2 Controller
let player2 = {

    instance: null,

    ready() {
        player2.instance = this;
        player2.update();
    },

    getSpeed() {
        return player2.instance.getSpeed();
    },

    update() {
        let fps = Math.round(100 * (1000 / player2.getSpeed())) / 100;
        document.querySelector(".speed").innerHTML = fps + ' fps';
    },

    increase() {
        let speed = Math.max(50, player2.getSpeed() - 50);
        player2.instance.setSpeed(speed);
        player2.update();
    },

    decrease() {
        let speed = Math.min(2000, player2.getSpeed() + 50);
        player2.instance.setSpeed(speed);
        player2.update();
    }
};


# Advanced techniques of controlling playback Speed

By default frames are played at a fixed speed specified with the initial parameters
It's actually not speed, but an interval between subsequent frames, specified
in milliseconds. The larger the interval, the slower the animation is.

If you need variable playback speed, there are two ways of manipulating it:

* by defining interval for each frame individually
* by calling `setSpeed` function while the player is playing
* by providing playback speed with easing function rather than fixed value

The first method is straightforward. You have seen it already in [Script](../01-script)
example, in the last animation on the page. In JSON file with frame definitions
we have individual speeds defined for each frame.

The second method is just as easy. In response to user actions or other events
you can call player's `setSpeed` method, which sets a new playback speed for all
frames to follow. You can call this method as many times as you want during playback.

The third method is more elaborate and more deterministic. Rather than a constant value,
you provide a function as a value of `frameSpeed` parameter (represented by `frame-speed`
DOM attribute or `frame speed` entry in playback script). The function receives the index
of current frame, the total number of frames and information whether we're now playing
in reverse order. Use these values to determine the speed for the specified frame.
You can implement all kinds of easing functions this way!
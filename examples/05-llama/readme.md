# Piskel animations
This example demonstrates how to use pixel art generated with online pixel-art editor `Piskel`,
found at [https://www.piskelapp.com/](https://www.piskelapp.com/).
Piskel can save your creation to many graphics formats, but also to a human- and machine-readable
source file in `C` programming language. It's a simple text file containing a definition of
array with RGB pixels - precisely what we need here!

The below examples were created by importing existing images into Piskel, then exporting them
to Piskel C format. Playback script points to that file as a source of frames, and that's all!
In the end, if you'd rather stick to the more human-friendlu pixel matrix format,
you can easily convert Piskel C files using our [Frame Converter](../converter) tool.

So much for the explanations, and now for something completely different:


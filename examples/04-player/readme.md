# Ani-Mate Video Player
A cool example of interactive animation player with playback controls. Here we use the JavaScript API
to start, pause and stop the animation. At any moment you can modify the script in the editor below, stop playing and start again with the new settings.

On the right you can see how busy the player is while playing. The script has `debug` mode set to `yes`.
We've subscribed to player's `debug` event to capture and display all other events as they happen.





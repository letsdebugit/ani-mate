let player = null;
let script = null;
let page = null;
let terminal = null;
let terminalLineCount = 0;
let buttons = null;

function ready() {
    player = this;
    page = document.querySelector('.page');
    script = document.querySelector('.player-script');
    terminal = document.querySelector('.player-terminal');
    buttons = {
        play: document.querySelector('#button-play'),
        stop: document.querySelector('#button-stop'),
        pause: document.querySelector('#button-pause'),
        resume: document.querySelector('#button-resume')
    };
    page.classList.add('visible');
    script.value = player.script();
    clearTerminal();
    updateButtons();
}

function playerAction(action) {
    if (player) {
        switch (action) {
            case 'play':
                if (player.isPaused()) {
                    player.resume();
                }
                else {
                    // Re-load script from the editor,
                    // so the user can interact directly in the page :-)
                    clearTerminal();
                    player
                        .configure({ scriptElement: '.player-script' })
                        .then(() => player.play())
                        .then(() => updateButtons())
                }
                break;
            case 'stop':
                if (player.isPlaying()) {
                    clearTerminal();
                    player.stop();
                }
                break;
            case 'pause':
                if (player.isPlaying()) {
                    player.pause();
                }
                break;
        }
        updateButtons();
    }
}

function updateButtons() {
    if (buttons) {
        buttons.play.classList.toggle('active', !player.isPlaying());
        buttons.stop.classList.toggle('active', player.isPlaying());
        buttons.pause.classList.toggle('active', player.isPlaying());
    }
}

function notify(message) {
    if (terminal && message && message.toString().trim() != '') {
        if (terminalLineCount >= 1000) {
            terminal.value = '';
        }
        terminal.value = message + '\n' + terminal.value;
    }
    updateButtons();
}

function log() {
    notify(this.logger().message);
}

function clearTerminal() {
    if (terminal) {
        terminal.value = '';
    }
}

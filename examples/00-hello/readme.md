# The simplest example of an embedded Ani-Mate Player

The simplest Ani-Mate Player ever.
Animation frames are defined inline in human-readable pixel matrix format.
Event handler for `stop` event is purely optional, used here
to demonstrate how easy it is to hook up into player's lifecycle events.

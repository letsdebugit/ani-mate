'use strict';

let player = null;
let counter = null;
let rawFrame = [
    [1, 0, 1, 0],
    [0, 1, 0, 1],
    [1, 0, 1, 0],
    [0, 1, 0, 1],
    [1, 0, 1, 0],
];

function ready(arg) {
    player = this;
    counter = document.querySelector('.frame-index');
    player.addEventListener('play', update);
    player.addEventListener('stop', update);
    player.addEventListener('frame', update);
    update();
};

function update() {
    if (player && counter) {
        counter.innerHTML = `Frame #${player.index() + 1}/${player.count()}`;
    }
}
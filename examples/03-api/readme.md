# Ani-Mate Player JavaScript API

Player API allows fine-grained programmatic control over the playback process.
Regardless of how you've initialized your player - with DOM attributes, script or programmatically,
you can interact with the player using a rich API.

As demonstrated below, you can just start playing by calling player's `play()` method.
You can also play frames one by one, play a subset of frames, play it again but with customized
settings, stop and resume playback, change playback speed or even play arbitrary frames!

Can you do that with your animated GIF?


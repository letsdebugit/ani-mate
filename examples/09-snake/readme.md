# Snake Game

Another frame generator example, used here to implement
the classic `Nokia Snake` game.

Use keyboard or phone buttons to control the game:

* `Call` button or `space` key - start / pause / resume the game
* `C` button or `C` key - restart the game
* `↑ ↓ ← →` arrow keys turn snake into specified direction
* `Down` and `Up` buttons on the phone turn snake depending on its current direction:
  - If snake moves left-right, `Down` will turn it down, `Up` will turn it up
  - If snake moves up-down, `Down` will move it left, `Up` will move it right

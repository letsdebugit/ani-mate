'use strict';

// Player controller
let Player = {
    // Ani-Mate instance
    instance: null,

    // Triggered when player is ready for action
    ready() {
        Player.instance = this;
        window.addEventListener('keydown', event => {
            if (Player.onKeyDown(event.keyCode, event.key)) {
                event.preventDefault();
                return false;
            };
        });
        document.querySelector('.button-call').addEventListener('click', () => Player.onButtonPressed('pause-resume'));
        document.querySelector('.button-c').addEventListener('click', () => Player.onButtonPressed('restart'));
        document.querySelector('.button-up').addEventListener('click', () => Player.onButtonPressed('up'));
        document.querySelector('.button-down').addEventListener('click', () => Player.onButtonPressed('down'));
        Player.displayStatus('ready');
    },

    // Button pressed event handler
    onButtonPressed: event => null,

    // Key down event handler
    onKeyDown: event => null,

    // Starts the game
    start() {
        document.querySelector('.controls').classList.add('visible');
        Player.displayStatus('');
        Player.instance
            .reset()
            .then(() => Player.instance.play());
        return true;
    },

    // Stops the game
    stop() {
        if (Player.instance.stop()) {
            Player.instance.clear();
            Player.displayStatus('ready');
            return true;
        }
    },

    // Pauses the game
    pause() {
        if (Player.instance.pause()) {
            Player.displayStatus('paused');
            return true;
        }
    },

    // Resumes the game
    resume() {
        if (Player.instance.resume()) {
            Player.displayStatus('');
            return true;
        }
    },

    // Starts/pauses/resumes game depending on its current state
    toggle() {
        if (Player.instance.isPaused()) {
            return Player.resume();
        }
        else if (Player.instance.isPlaying()) {
            return Player.pause();
        }
        else {
            return Player.start();
        }
    },

    // The snake dies
    die() {
        if (Player.instance.pause()) {
            Player.displayStatus('dead');
            window.setTimeout(() => Player.stop(), 2000);
        }
    },

    // Displays the current score
    displayScore(score) {
        let text = '0'.repeat(4 - score.toString().length) + score.toString();
        document.querySelector('.score').innerText = text;
    },

    // Displays the current speed
    displaySpeed(speed) {
        document.querySelector('.speed').innerText = speed;
    },

    // Displays the status of the game
    displayStatus(status) {
        let states = {
            ready: 'Press CALL to start',
            paused: 'Press CALL to resume',
            dead: 'Aaaaaaarrghhhh...'
        };
        let element = document.querySelector('.status');
        element.innerText = states[status] || '';
        if (element.innerText) {
            element.classList.add('visible');
        }
        else {
            element.classList.remove('visible');
        }
    },

    // Frame generator
    snake(parameters) {

        // Movement directions
        let Directions = {
            n: { x: 0, y: -1, isVertical: true, opposite: 's' },
            e: { x: 1, y: 0, isHorizontal: true, opposite: 'w' },
            w: { x: -1, y: 0, isHorizontal: true, opposite: 'e' },
            s: { x: 0, y: 1, isVertical: true, opposite: 'n' }
        };

        // Wall boundaries
        let Walls = {
            left: { x: -1 },
            top: { y: -1 },
            right: { x: parameters ? parameters.columns : undefined },
            bottom: { y: parameters ? parameters.rows : undefined }
        };

        // Pixels and colors
        let Colors = {
            nothing: 0,
            snake: 1,
            food: 2,
            dead: 3
        };

        // Snake controller
        let Snake = {
            body: [],
            food: [],
            direction: null,
            velocity: 1,
            foodCounter: 1,
            dead: false,

            // Initializes the snake
            initialize(generator, frame) {
                Snake.dead = false;
                Snake.generator = generator;
                Snake.frame = frame;
                Snake.body = [{
                    x: Math.floor(parameters.columns / 2) - 1,
                    y: Math.floor(parameters.rows / 2)
                }];
                Snake.head(generator.randomItem(Object.keys(Directions)));
                Player.displayScore(Snake.body.length - 1);
                Player.displaySpeed(Snake.velocity);
            },

            // Renders game frame
            render: frame => {
                // drop food at specified intervals
                if (!Snake.dead) {
                    Snake.foodCounter++;
                    if (Snake.foodCounter >= parameters.foodFrequency) {
                        Snake.dropFood();
                        Snake.foodCounter = 1;
                    }
                }
                // Render snake and food
                frame.clear();
                frame.setPixels(Snake.body, Snake.dead ? Colors.dead : Colors.snake);
                frame.setPixels(Snake.food, Colors.food);
                return frame;
            },

            // Sets snake direction
            head: direction => direction ? Snake.direction = direction : undefined,

            // Moves the snake following the current direction
            move: direction => {
                if (Snake.dead) {
                    return;
                }
                Snake.head(direction);
                let movement = Directions[Snake.direction];
                let head = Snake.body[0];
                let tail = Snake.body[Snake.body.length - 1];
                if (head && movement) {
                    // Poll the new position
                    let x = head.x + movement.x;
                    let y = head.y + movement.y;
                    // If snake's own ass or wall at the new position, die
                    if (Snake.isSnakeAt(x, y) || Snake.isWallAt(x, y)) {
                        Snake.die();
                    }
                    // If food, swallow and grow
                    else if (Snake.isFoodAt(x, y)) {
                        Snake.swallow(x, y);
                    }
                    // Otherwise just keep moving
                    else {
                        // Clear tail
                        Snake.frame.setPixel(tail.x, tail.y, Colors.nothing);
                        // Add head at new position
                        Snake.body.splice(Snake.body.length - 1);
                        Snake.body = [{x: x, y: y}].concat(Snake.body);
                    }
                    return Snake;
                }
            },

            // Turns the snake towards specified direction
            turn: where => {
                let direction = Directions[where];
                let current = Directions[Snake.direction];
                if (current && direction) {
                    // if turning back, revert the snake, otherwise it will
                    // bite itself and die
                    if (where == current.opposite) {
                        Snake.body.reverse();
                    }
                    Snake.head(where);
                    return Snake;
                }
            },

            // Returns true if snake moves horizontally
            movesHorizontally: () => (Directions[Snake.direction] || {}).isHorizontal,

            // Returns true if snake moves vertically
            movesVertically: () => (Directions[Snake.direction] || {}).isVertical,

            // Checks whether there's a wall at the specified position
            isWallAt: (x, y) => {
                for (let wall of Object.values(Walls)) {
                    if (wall.x == x || wall.y == y) {
                        return true;
                    }
                }
            },

            // Checks whether there's a food crate at the specified position
            isFoodAt: (x, y) => {
                return Snake.food.some(pixel => pixel.x == x && pixel.y == y);
            },

            // Checks whether there's a snake at the specified position
            isSnakeAt: (x, y) => {
                return Snake.body.some(pixel => pixel.x == x && pixel.y == y);
            },

            // Checks whether there's nothing in particular at the specified position.
            // Well, even in nothingness there are always virtual particles coming and
            // going out of nowhere, but that's irrelevant here.
            isNothingAt: (x, y) => {
                return (!(Snake.isFoodAt(x, y) || Snake.isSnakeAt(x, y)));
            },

            // Drops a food crate at random location
            dropFood: () => {
                if (Snake.food.length < parameters.foodCount) {
                    let location;
                    while (!(location = Snake.frame.getRandomPixel((x, y) => Snake.isNothingAt(x, y)))) {
                    };
                    Snake.food.push({
                        x: location.x,
                        y: location.y
                    });
                }
            },

            // Swallows the food crate at specified location and grows the snake
            swallow: (x, y) => {
                let i = Snake.food.findIndex(f => f.x == x && f.y == y);
                if (i > -1) {
                    Snake.body = [{ x: x, y: y}].concat(Snake.body);
                    Snake.food.splice(i, 1);
                    Player.displayScore(Snake.body.length - 1);
                    if (Snake.body.length % parameters.accelerationFrequency == 0) {
                        Snake.accelerate();
                    }
                }
            },

            // Accelerates the snake
            accelerate: () => {
                let speed = Math.max(50, Player.instance.getSpeed() - parameters.accelerationRate);
                Player.instance.setSpeed(speed);
                Snake.velocity++;
                Player.displaySpeed(Snake.velocity);
            },

            // The snake bites its own ass and dies
            die: (x, y) => {
                Snake.dead = true;
            },
        };

        // Frame representing the game screen
        let Frame = null;

        // Frame generator
        let Generator = {
            // Number of frames is not determined
            isFinite: () => false,

            onReady: () => {
                Player.onKeyDown = Generator.onKeyDown;
                Player.onButtonPressed = Generator.onButtonPressed;
            },

            onStart: () => {
                Frame = Generator.createFrame(0);
                Snake.initialize(Generator, Frame);
            },

            // Triggered when key pressed
            onKeyDown: (keyCode, key) => {
                let handled = false;
                switch (keyCode) {
                    // Arrow keys
                    case 37: handled = Snake.turn('w'); break;   // left arrow
                    case 38: handled = Snake.turn('n'); break;   // up arrow
                    case 39: handled = Snake.turn('e'); break;   // right arrow
                    case 40: handled = Snake.turn('s'); break;   // down arrow
                    // P - pause
                    case 80: handled = Player.pause(); break;
                    // R - resume
                    case 82: handled = Player.resume(); break;
                    // Space - pause/resume
                    case 32: handled = Player.toggle(); break;
                    // C - restart
                    case 67: handled = Player.start(); break;
                };
                return handled;
            },

            // Triggered when phone button pressed
            onButtonPressed: action => {
                switch (action) {
                    case 'up': Snake.movesHorizontally() ? Snake.turn('n') : Snake.turn('e'); break;
                    case 'down': Snake.movesHorizontally() ? Snake.turn('s') : Snake.turn('w'); break;
                    case 'pause-resume': Player.toggle(); break;
                    case 'restart': Player.start(); break;
                }
            },

            // Returns the frame at specified index
            getFrame: index => {
                Snake.move();
                if (Snake.dead) {
                    Player.die();
                }
                return Snake.render(Frame);
            },

        };

        return Generator;
    }

};




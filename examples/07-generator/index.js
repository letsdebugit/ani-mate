// Player Controller
let player = {

    // Frame generator creating a 'walking pixel' frame
    walkingPixel(parameters) {

        let generator = {
            // The number of frames equals number of columns
            getCount: () => generator.getColumns(),
            // Generate frame at index - create an empty frame filled with zero's,
            // then place 1 at appropriate index
            getFrame: index => generator.createFrame(0, [{ x: index, y: 0, color: 1 }])
        }

        return generator;
    },

    // Frame generator creating a 'scrolling text' frame
    textScroller(parameters) {
        let emptyFrame = null;
        let text = 'This is awesome! ';

        let generator = {
            isFinite: () => false,
            getFrame: () => emptyFrame = emptyFrame || generator.createFrame(0),
            getPixelContent: (frame, x, y, pixel) => {
                let start = generator.getIndex() % text.length;
                let chx = (start + x) % text.length;
                return text[chx];
            }
        }
        return generator;
    }

};


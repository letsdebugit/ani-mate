PARAMETERS
    debug no
    width 100%
    height 50px
    rows 1
    columns 10
    grid 0
    autoplay yes
    loop yes
    frame speed 200
    frame smooth 150

COLORS
    0 #ffffff
    1 #5fa7e7


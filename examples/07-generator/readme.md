# Frame generators

Rather than explicitly specifying a fixed set of frames,
you can render them dynamically using a custom frame generator.
Frame generator is an object implementing certain set of functions
providing all necessary information to the renderer. At minimum,
it has to implement `getCount()` function informing about the number
of frames to play and `getFrame()` function returning a frame at specified
index.

In some scenarios the number of frames cannot be determined beforehand.
Frame generator implementing the [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) in theory could run
indefinitely. To implement such frame generators you need a more advanced strategy.
Ignore `getCount()` and inform the player that length of  frame sequence is indetermined,
by implementing `isFinite()` method which returns `false`. Then, the best strategy
is to implement rendering of the next frame in `onNext()` event handler. This event
handler is fired whenever the player is about to render another frame in sequence.
You can see how this works in another example of ours,: [Guess the Picture Game](../08-guess).
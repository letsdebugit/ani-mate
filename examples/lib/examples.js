let AniMateExamples = {

    repository: 'https://bitbucket.org/letsdebugit/ani-mate/src/master/examples/',

    initialize(container) {
        if (container) {
            let example = container.getAttribute('example');
            if (example) {
                return fetch('readme.md')
                    .then(response => response.text())
                    .then(text => {
                        container.innerHTML = text ? markdown.toHTML(text) : '';
                        AniMateExamples.addSourceLink(container, example);
                    })
                    .catch(() => Promise.resolve());
            }
        }
        return Promise.resolve();
    },

    getExampleSourceRepository(example) {
        if (example) {
            return AniMateExamples.repository + example;
        }
    },

    addSourceLink(container, example) {
        container = container ? container.querySelector('h1') : null;
        if (container && example) {
            let buttonContainer = document.createElement('div');
            buttonContainer.className = 'source-button-container';
            let button = document.createElement('button');
            button.className = "source-button";
            button.innerText = "Show sources ...";
            button.addEventListener('click', () => {
                let url = AniMateExamples.getExampleSourceRepository(example);
                if (url) {
                    window.open(url, '_blank');
                }
            });
            buttonContainer.appendChild(button);
            container.appendChild(buttonContainer);
        }
    }

};

window.addEventListener('load', () => {
    let exampleContainer = document.querySelector('*[example]');
    AniMateExamples
        .initialize(exampleContainer)
        .then(() => document.body.className = 'visible')
});
# Player initialized with JavaScript code

Rather than using DOM attributes or playback scripts, you can configure the player using
JavaScript code. Using `data-configuration` attribute you can instruct the player to fetch
its configuration from the specified JavaScript variable or function. The returned object
must contain playback parameters, palette and frames.

The familiar `addEventListener` method is used to subscribe to notifications about playback events such as start, stop or next frame being played.

As a bonus, we demonstrate her how to completely customize the look and feel of animation
pixels, modifying the CSS classes responsible for pixel look and feel.

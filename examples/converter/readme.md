# Frame Converter
This utility converts animations in other formats (Piskel C, GIF) to native Frame Matrix definitions,
for easier embedding and manipulation.


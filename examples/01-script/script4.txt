PARAMETERS
    width 200px
    height 200px
    columns 5
    rows 5
    grid 1px
    autoplay yes
    loop yes
    autoreverse yes

COLORS
    background #ffffda
    grid #e0e0e0
    0 #ffffda
    1 #d0d0d0
    2 #b0b0b0
    3 #909090
    4 #707070
    5 #505050
PARAMETERS
    width 200px
    height 200px
    columns 5
    rows 5
    grid 1px
    autoplay yes
    loop yes
    autoreverse yes
    frame format csv
    frame speed 150
    frame smooth 50

COLORS
    background #ffffda
    grid #e0e0e0
    0 #ffffda

FRAMES
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #303030, #303030, #303030, #303030, #303030,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #606060, #606060, #606060, #606060, #606060, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, #909090, #909090, #909090, #909090, #909090, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, #b0b0b0, #b0b0b0, #b0b0b0, #b0b0b0, #b0b0b0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #d0d0d0, #d0d0d0, #d0d0d0, #d0d0d0, #d0d0d0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0


(function () {

'use strict';

/*
    AniMate Player
*/
function AniMatePlayer(container) {

    // Executor, for evaluating expressions and calling functions
    let Executor = {
        context: null,

        initialize(context) {
            if (!context) {
                throw new Error('Execution context must be specified');
            }
            Executor.context = context;
        },

        // Calls the specified event handler within the specified execution context
        execute(expression, context) {
            context = context || Executor.context;
            if (typeof expression === "function") {
                return expression.bind(context)();
            }
            else {
                expression = (expression || '').trim();
                if (expression) {
                    let args = (expression.match(/(?<=\().+?(?=\))/gi) || [])[0];
                    expression = expression.replace(/\(.*?\)/gi, `.bind(this)(${args || ''});`);
                    let fn = new Function(`return ${expression};`);
                    return fn.call(context);
                }
            }
        },

        // Converts the expression into function, executed within the specified execution context
        unwrap(expression, context) {
            if (expression) {
                context = context || Executor.context;
                let argString = (expression.match(/(?<=\().+?(?=\))/gi) || [])[0] || '';
                let args = argString
                    .split(',')
                    .map(a => (a || '').trim())
                    .filter(a => !Executor.isValueArgument(a));
                let fn = new Function(...args, `return ${expression};`);
                return fn.bind(context);
            }
        },

        // Evaluates the specified expression within the specified execution context
        evaluate(expression, context) {
            let fn = Executor.unwrap(expression, context);
            if (fn && typeof fn == 'function') {
                context = context || Executor.context;
                return fn.call(context);
            }
        },

        // Checks if the specified function argument is not a value argument
        isValueArgument(a) {
            if (a) {
                return (a == '') ||
                    (!isNaN(parseFloat(a))) ||
                    (a === 'true') ||
                    (a === 'false') ||
                    (a[0] == '"') ||
                    (a[0] == "'") ||
                    (a[0] == '`');
            }
        },

        // Checks whether the specified expression is a function call
        isFunctionCall(expression) {
            return (expression || '') != '' && expression.indexOf('(') > -1 && expression.indexOf(')') > expression.indexOf('(');
        }

    };

    // Internal settings of the player
    let Settings = {
        // Initialization flag, set after all settings have been succesfully parsed
        isInitialized: false,
        // Animation frames / frame generator
        frames: {
            items: null,
            generator: null
        },
        // Playback parameters
        parameters: {
            name: null,           // Player name
            width: 0,             // Frame width, default in pixels but other units are allowed.
            height: 0,            // Frame height, default in pixels but other units are allowed.
            grid: 1,              // Grid lines width in pixels
            rows: 0,              // Number of rows in a frame
            columns: 0,           // Number of columns in a frame
            autoplay: true,       // If true, the player starts playing the animation immediately after the page has loaded
            loop: false,          // If true, animation will be repeated indefinitely or the specified number of times
            reverse: false,       // If true, animation will be played in reverse order
            autoreverse: false,   // If true, looping animation goes backwards on reaching the end
            frameUrl: null,       // URL where frames script is to be fetched from
            frameSpeed:  0,       // Interval between frames: number of milliseconds or easing function
            frameFormat: null,    // Frame file format
            frameSmooth: 0,       // Duration of CSS transitions when changing pixel colors, used for smoother animations
            script: null,         // Playback script to play
            scriptUrl: null,      // URL where the playback script can be fetched from
            scriptElement: null,  // Selector of DOM element within which the playback script is stored
        },
        // Color palette
        palette: {},
        // Event hooks
        events: {
            ready: null,
            play: null,
            stop: null,
            pause: null,
            resume: null,
            reverse: null,
            move: null,
            frame: null,
            loop: null,
            error: null,
            log: null
        },
        // Rendering canvas
        canvas: null,
        // Settings store
        storage: {},

        // Initializes the state with parameters, frames and other configuration settings
        initialize: (parameters, frames, events, palette, canvas) => {
            Settings.parameters = Object.assign(Settings.parameters, parameters || {});
            Settings.events = Object.assign(Settings.events, events || {});
            Settings.palette = Object.assign(Settings.palette, palette || {});
            Settings.canvas = Settings.canvas || canvas;
            if (Settings.isFunction(frames)) {
                Settings.frames.generator = frames;
            }
            else if (Settings.isFrameList(frames)) {
                // Add metadata to frames
                Settings.frames.items = frames
                    .map((data, i) => Frame(i, data, Settings.parameters.columns, Settings.parameters.rows));
            }
        },

        // Stores the current parameters
        store() {
            Object.assign(Settings.storage, Settings.parameters);
            return Settings.storage;
        },

        // Restores the previously stored parameters
        restore() {
            Object.assign(Settings.parameters, Settings.storage || {});
            return Settings.parameters;
        },

        // Returns true if specified object is a function
        isFunction: instance => instance && typeof instance == 'function',

        // Returns true if frames are specified as generator function
        isFrameGenerator: instance => instance && instance.getFrame && typeof instance.getFrame == 'function',

        // Returns true if frames are specified as simple list
        isFrameList: instance => instance && typeof instance != 'function' && instance.length != undefined,
    };

    // Logger activated in debug mode
    let Logger = {
        enabled: false,
        message: null,
        isError: false,
        data: null,

        // Initializes the logger
        initialize(enabled) {
            Logger.enabled = enabled;
            return Logger;
        },

        // Logs a message
        info(message, data) {
            if (Logger.enabled) {
                Logger.message = message;
                Logger.isError = false;
                Logger.data = data;
                data != undefined ? console.info(message, data) : console.info(message);
                Executor.execute(Settings.events.log);
            }
        },

        // Logs a warning
        warn(message, data) {
            if (Logger.enabled) {
                Logger.message = message;
                Logger.isError = false;
                Logger.data = data;
                data != undefined ? console.warn(message, data) : console.warn(message);
                Executor.execute(Settings.events.log);
            }
        },

        // Logs an error
        error(message, data) {
            if (Logger.enabled) {
                Logger.message = message;
                Logger.isError = true;
                Logger.data = data;
                data != undefined ? console.error(message, data) : console.error(message);
                Executor.execute(Settings.events.log);
            }
        }
    };

    // Script parser
    let ScriptParser = {
        // Supported frame formats
        supportedFormats: ['default', 'json', 'csv', 'piskel'],

        // Parser state
        state: {
            parameters: null,
            frames: null,
            palette: null,
            inParameters: false,
            inColors: false,
            reset() {
                this.inParameters = false;
                this.inColors = false;
            },
            addParameter(line, prefix) {
                let values = line.split(' ').map(v => (v || '').trim());
                if (prefix) {
                    values = [prefix].concat(values);
                }
                let key = values
                    .slice(0, values.length - 1)
                    .map((s, i) => i == 0 ? s : s[0].toUpperCase() + s.substr(1))
                    .join('');
                let value = values[values.length - 1].trim();
                if (key != '' && value != '') {
                    if (!this.parameters) {
                        this.parameters = {};
                    }
                    if (isNaN(value)) {
                        value = value.toLowerCase();
                        if (value == "yes" || value == "no") {
                            this.parameters[key] = (value == "yes");
                        }
                        else {
                            this.parameters[key] = value;
                        }
                    }
                    else {
                        this.parameters[key] = parseInt(value);
                    }
                    return {
                        key: key,
                        value: this.parameters[key]
                    }
                }
            },
            addColor(line) {
                let values = line.split(' ').map(v => (v || '').trim());
                if (values[0] != '' && values[1] != '') {
                    if (!this.palette) {
                        this.palette = {};
                    }
                    this.palette[values[0]] = values[1];
                }
            },
        },

        // Parses play script
        parseScript(script) {
            if (!script || script.trim() == '') {
                return;
            }
            let lines = script.split('\n');
            let lineNo = 0;
            this.state.reset();
            while (lineNo < lines.length) {
                let line = lines[lineNo].trim();
                lineNo++;
                if (line[0] == '#') {
                    continue;
                }
                if (line.toLowerCase() == 'parameters') {
                    this.state.reset();
                    this.state.inParameters = true;
                    continue;
                }
                else if (line.toLowerCase() == 'colors') {
                    this.state.reset();
                    this.state.inColors = true;
                    continue;
                }
                if (line.toLowerCase() == 'frames') {
                    this.state.frames = this.parseFrames(
                        this.state.parameters.frameFormat,
                        this.state.parameters.columns,
                        this.state.parameters.rows,
                        lines.slice(lineNo).join('\n'));
                    lineNo = lines.length;
                    continue;
                }
                else {
                    if (this.state.inParameters && line != '') {
                        this.state.addParameter(line);
                    }
                    else if (this.state.inColors && line != '') {
                        this.state.addColor(line);
                    }
                }
            }

            let result = {
                parameters: this.state.parameters,
                frames: this.state.frames,
                palette: this.state.palette,
                styles: this.state.styles
            };
            Logger.info('Script parsed', result);
            return result;
        },

        // Parses frames script
        parseFrames(format, columns, rows, script) {
            // Store columns and rows, but this might be overridden by frame parser
            ScriptParser.state.parameters.columns = columns;
            ScriptParser.state.parameters.rows = rows;

            // Determine and validate frame format
            format = (format || ScriptParser.state.parameters.frameFormat || 'default').toLowerCase();
            if (ScriptParser.supportedFormats.indexOf(format) == -1) {
                throw new Error('Unsupported frame format', format);
            }

            // Parse frames, if present in the script
            let frames = [];
            if (script && script.trim() != '') {
                switch (format) {
                    case 'default':
                        frames = this.defaultFrameParser(script, columns, rows);
                        break;
                    case 'json':
                        frames = this.jsonFrameParser(script, columns, rows);
                        break;
                    case 'csv':
                        frames = this.csvFrameParser(script, columns, rows);
                        break;
                    case 'piskel':
                        frames = this.piskelFrameParser(script, columns, rows);
                        columns = ScriptParser.state.parameters.columns || columns;
                        rows = ScriptParser.state.parameters.rows || rows;
                        break;
                    default:
                        throw new Error(`Unsupported frame format ${format}`);
                        break;
                }
            }

            // Add metadata to frames
            if (frames) {
                frames = frames.map((data, i) => Frame(i, data, columns, rows));
            }

            return frames.length > 0 ? frames : null;
        },

        // Parses frames specified in default tabular format
        defaultFrameParser(script, columns, rows) {
            Logger.info('Parsing frames in default format');
            let frames = [];
            let pixels = [];
            let id = null;
            let speed = null;

            function addFrame(id, pixels, speed) {
                frames.push(Frame(id, pixels, columns, rows, isNaN(speed) ? null : speed));
            }

            for (let line of script.split('\n')) {
                line = line.trim();
                if (line == '') {
                    if (pixels.length > 0) {
                        addFrame(id, pixels, speed);
                    }
                    pixels = [];
                    continue;
                }
                else if (line.indexOf('id ') == 0) {
                    id = line.substr(3).trim();
                    continue;
                }
                else if (line.indexOf('speed ') == 0) {
                    speed = parseInt(line.substr(6).trim());
                    continue;
                }
                else {
                    pixels.push(line.split(' '));
                }
            }
            if (pixels.length > 0) {
                addFrame(id, pixels, speed);
            }
            return frames;
        },

        // Parses frames specified in JSON format
        jsonFrameParser(script, columns, rows) {
            Logger.info('Parsing frames in JSON format');
            try {
                let frames = JSON.parse(script);
                return Object
                    .keys(frames)
                    .map(id => Frame(id, frames[id], columns, rows));
            }
            catch (e) {
                throw new Error('Error parsing frames in JSON format', e.toString());
            }
        },

        // Parses frames specified in comma-separated format
        csvFrameParser(script, columns, rows) {
            Logger.info('Parsing frames in comma-separated format');
            if (script) {
                let items = script
                    .trim()
                    .split('\n')
                    .map(line => line.trim())
                    .join('')
                    .split(',')
                    .map(item => item.trim())
                let index = 0;
                let frames = [];
                let frame = [];
                let row = null;
                while (items.length > 0) {
                    row = items.splice(0, columns);
                    frame.push(row);
                    if (frame.length == rows) {
                        frames.push(frame);
                        frame = [];
                    }
                }
                if (frame.length > 0) {
                    frames.push(frame);
                }
                return frames;
            }
        },

        // Parses frames specified in Piskel C format
        piskelFrameParser(script, columns, rows) {
            Logger.info('Parsing frames in Piskel C format');
            if (script) {
                // extract frame size
                if (!columns) {
                    columns = parseInt((script.match(/(?<=FRAME_WIDTH\s)(.*?)(?=\r|\n)/g) || '')[0]);
                    if (!isNaN(columns)) {
                        ScriptParser.state.parameters.columns = columns;
                    }
                }
                if (!rows) {
                    rows = parseInt((script.match(/(?<=FRAME_HEIGHT\s)(.*?)(?=\r|\n)/g) || '')[0]);
                    if (!isNaN(rows)) {
                        ScriptParser.state.parameters.rows = rows;
                    }
                }
                // extract pixels
                let frames = [];
                script = script.substr(script.indexOf('{') + 1).trim();
                script = script.substring(0, script.indexOf('};'));
                script = script.replace(/\{/gmi, '[');
                script = script.replace(/\}/gmi, ']');
                script = `return [${script}];`;
                let fnFrames = new Function(script);
                frames = fnFrames();
                frames = frames
                    .map(frame => frame
                        .map(x => {
                            x = x.toString(16);
                            if (x.length == 8)
                                return '#' + x.substr(6, 2) + x.substr(4, 2) + x.substr(2, 2);
                            else
                                return x;
                        }));
                frames = frames
                    .map(frame => {
                        let rows = [];
                        let i = 0;
                        while (i < frame.length) {
                            rows.push(frame.slice(i, i + columns));
                            i = i + columns;
                        }
                        return rows;
                    });
                return frames;
            }
        },

        // Creates colour palette using pixels with hard-coded colours
        createPalette(frames) {
            let palette = null;
            if (frames && Settings.isFrameList(frames)) {
                let colormap = {
                    count: 0,
                    colors: {},
                    keys: {},
                    pixels: [],
                };
                const INTERNAL_COLOR_ID = 1000;
                const MAX_PALETTE_ENTRIES = 100;

                // Collect colours
                for (let f = 0; f < frames.length; f++) {
                    const pixels = frames[f].data || [];
                    for (let y = 0; y < pixels.length; y++) {
                        const row = pixels[y] || [];
                        for (let x = 0; x < row.length; x++) {
                            const pixel = row[x];
                            if (pixel && pixel[0] === '#') {
                                let key;
                                if (!(key = colormap.keys[pixel])) {
                                    key = 'c_' + (INTERNAL_COLOR_ID + colormap.count++);
                                    colormap.colors[key] = pixel;
                                    colormap.keys[pixel] = key;
                                }
                                colormap.pixels.push({ f: f, y: y, x: x, key: key });
                            }
                        }
                    }
                }

                // Only convert to palette if less than the maximal specified amount of colours
                if (colormap.count > 0) {
                    if (colormap.count <= MAX_PALETTE_ENTRIES) {
                        for (let pixel of colormap.pixels) {
                            frames[pixel.f].data[pixel.y][pixel.x] = pixel.key;
                        }
                        palette = Object.assign({}, colormap.colors);
                    }
                }
            }
            return palette;
        },

    };

    // Frame
    function Frame(id, data, columns, rows, speed)  {
        if (data && data.data) {
            // Frame object?
            if (typeof data.hasData == 'function') {
                return data;
            }
            // Enriched frame data? Turn into a frame object.
            else {
                return Frame(data.id || id, data.data, data.columns || columns, data.rows || rows, data.speed || speed);
            }
        }
        else {
            // Just raw pixels, so wrap it up in a frame object
            let frame = {
                // Frame identifier
                id: id,
                // Set to true to disable the frame
                disabled: false,
                // Frame speed
                speed: speed,
                // Frame size
                columns: columns,
                rows: rows,
                // Frame pixels, a two-dimensional array of colors or palette references
                data: data,
                // Pixels to update when transitioning between frames (in both directions)
                diff: {
                    index: null,
                    forward: null,
                    reverse: null,
                    ratio: { forward: 0, reverse: 0 }
                },
                // Returns true if frame contains any pixels
                hasData: () => frame.data && frame.data.length > 0,
                // Returns true if forward playback diff data is available for transitioning into the frame
                hasForwardDiff: () => frame.diff  && frame.diff.forward,
                // Returns true if reverse playback diff data is available for transitioning into the frame
                hasReverseDiff: () => frame.diff  && frame.diff.reverse,
                // Returns row at specified index
                getRow: index => (frame.data || [])[index] || [],
                // Returns the color of the specified pixel
                getPixel: (x, y) => frame.getRow(y)[x],
                // Returns a list of all set pixels, eventually only those with specified color
                getPixels: color => (frame.data || [])
                        .reduce((pixels, row, y) => pixels.concat(
                            row.map((color, x) => { return {x: x, y: y, color: color};} )), [])
                        .filter(pixel => color == undefined || pixel.color == color),
                // Returns a random pixel in the frame, optionally matching the specified predicate
                getRandomPixel: predicate => {
                    let pixel;
                    while (!pixel) {
                        let y = Math.floor((frame.data || []).length * Math.random());
                        let row = frame.getRow(y);
                        let x = Math.floor(row.length * Math.random());
                        if (!predicate || predicate(x, y, row[x])) {
                            return {
                                x: x,
                                y: y,
                                color: row[x]
                            };
                        }
                    }
                },
                // Clears the frame
                clear: () => frame.fill(),
                // Fills the frame with specified color
                fill: color => Array.from({ length: frame.rows }).map(row => Array.from({ length: frame.columns }).map(pixel => color)),
                // Changes color of the specified pixel
                setPixel: (x, y, color) => frame.getRow(y)[x] = color,
                // Clears the specified pixel
                clearPixel: (x, y) => frame.setPixel(x, y),
                // Changes colors of the specified pixels
                setPixels: (pixels, color) => pixels.forEach(pixel => frame.setPixel(pixel.x, pixel.y, color)),
                // Clears the specified pixels
                clearPixels: pixels => frame.setPixels(pixels),
            };
            return frame;
        }
    };

    // Generic frame generator.
    // You can override default behaviour by providing alternative implementations
    // of methods and properties in `instance` object
    function FrameGenerator(generator, parameters) {

        let index = 0;
        let direction = 1;

        let base = {
            // CAPABILITIES
            // Returns true if the number of frames produced by generator is finite and deterministic
            isFinite: () => true,
            // Returns true if generator allows pre-rendering of all frames before they're played, for speedier animations
            canPrerender: () => false,
            // Returns true if random jumping forward through frames is allowed
            canMove: () => true,
            // Returns true if random jumping backwards through frames is allowed
            canMoveBack: () => true,
            // Returns true if playback order can be reversed
            canReverse: () => true,
            // Returns true if playback can be repeated over and over
            canLoop: () => true,
            // Returns true if generator allows rendering arbitrary frames, outside the playback sequence
            canPlayRaw: () => true,

            // PROPERTIES
            // Returns the number of frame columns (horizontal pixels)
            getColumns: () => parameters.columns,
            // Returns the number of frame rows (vertical pixels)
            getRows: () => parameters.rows,
            // Returns the index of the current frame
            getIndex: () => index,
            // Returns the movement direction
            getDirection: () => direction,
            // Returns true if current frame is the first one in the currently played sequence
            isFirst: () => generator.isReversed() ? generator.getIndex() >= generator.getCount() - 1 : generator.getIndex() <= 0,
            // Returns true if current frame is the last one in the currently played sequence
            isLast: () => !generator.isReversed() ? generator.getIndex() >= generator.getCount() - 1 : generator.getIndex() <= 0,
            // Returns true if playback direction has been reversed
            isReversed: () => generator.getDirection() == -1,
            // Returns true if there are no frames to play
            isEmpty: () => generator.getCount() == 0,
            // Returns the frame at specified index - must implement in actual generators
            getFrame: at => { throw new Error('Not implemented'); },
            // Returns the total number of frames, must be implemented in finite generators
            getCount: () => { if (generator.isFinite()) { throw new Error('Not implemented') } else { return Number.MAX_VALUE; }},
            // Returns the current frame
            getCurrentFrame: () => generator.getFrame(index),
            // Returns the first frame in the sequence
            getFirstFrame: () => generator.getFrame(generator.isReversed() ? generator.getCount() - 1 : 0),
            // Returns the last frame in the sequence
            getLastFrame: () => generator.getFrame(!generator.isReversed() ? generator.getCount() - 1 : 0),
            // Returns the color or palette key of the specified pixel in the given raw frame
            getPixel: (pixels, x, y) => (pixels[y] || [])[x],
            // Returns HTML content of the specified pixel in the given raw frame
            getPixelContent: (pixels, x, y, pixel) => null,

            // API
            // Resets the generator to start position
            reset: () => index = 0,
            // Goes to the specified frame (doesn't play the frame, just moves the current frame pointer)
            goTo: to => index = Math.max(0, Math.min(to, generator.getCount() - 1)),
            // Goes to the next frame
            goForward: steps => generator.skip(generator.getDirection() * (steps || 1)),
            // Goes to the previous frame
            goBack: steps => generator.skip(-generator.getDirection() * (steps || 1)),
            // Goes to the first frame
            goFirst: () => generator.isReversed() ? generator.goTo(generator.getCount() - 1) : generator.goTo(0),
            // Goes to the last frame
            goLast: () => !generator.isReversed() ? generator.goTo(generator.getCount() - 1) : generator.goTo(0),
            // Moves the frame pointer by specified delta
            skip: delta => {
                generator.goTo(generator.getIndex() + generator.getDirection() * delta);
                if (delta == 1) generator.onNext();
            },
            // Reverses playback direction
            reverse: () => direction = -direction,
            // Creates an empty frame, fills with specified color if specified, then sets the specified pixels
            createFrame: (fill, pixels) => {
                let frame = Array.from({ length: parameters.rows }).map(row => Array.from({ length: parameters.columns }).map(pixel => fill));
                if (pixels) {
                    for (let pixel of pixels) {
                        (frame[pixel.y] || [])[pixel.x] = pixel.color;
                    }
                }
                return Frame(null, frame, generator.getColumns(), generator.getRows(), null);
            },

            // CALLBACKS
            // Called when player has been initialized
            onReady: () => {},
            // Called when player is about to start playing
            onStart: () => { },
            // Called when player has finished playing
            onStop: () => { },
            // Called when next frame in the sequence is about to be played
            onNext: () => { },
            // Called when frame has been rendered
            onRender: () => { },

            // UTILITY FUNCTIONS
            // Returns a random integer within the specified range: min <= n < max
            random: (min, max) => Math.floor(Math.random() * (max - min)),
            // Returns a random item from the specified array
            randomItem: items => items ? items[generator.random(0, items.length)] : undefined
        };

        if (generator) {
            generator.base = {};
            // Keep overridden methods in the generator intact,
            // inject all the rest of base methods.
            for (let key of Object.keys(base)) {
                if (generator[key] == undefined) {
                    generator[key] = base[key];
                }
                else {
                    // Store the base overridden methods in .base property,
                    // just in case the descendant needs them
                    generator.base[key] = base[key];
                }
            }
        }
        else {
            generator = base;
        }

        return generator;
    }

    // Frame generator factory
    let FrameGenerators = {

        // Creates a frame generator
        createGenerator(parameters, frames, sequence) {
            let instance = null;
            // Predefined list of frames?
            if (frames.items) {
                // Wrap into default frame generator
                instance = FrameGenerators.defaultGenerator(
                    parameters,
                    FrameGenerators.createSequence(frames.items, sequence, parameters.reverse));
            }
            // Frame generator function?
            else if (frames.generator) {
                instance = FrameGenerators.customGenerator(
                    parameters,
                    frames.generator);
            }
            if (!instance) {
                throw new Error('Sequence of frames to play is not specified');
            }
            instance.onReady();
            Logger.info('Frame generator created', instance);
            return instance;
        },

        // Default frame generator, which implements a bi-directional list of
        // predefined frames with random access in both directions, looping and reverse playback
        defaultGenerator(parameters, frameList) {
            if (!Settings.isFrameList(frameList)) {
                throw new Error('The provided value is not a frame list', frameList)
            }
            return FrameGenerator(
                {
                    canPrerender: () => true,
                    getCount: () => frameList.length,
                    getFrame: at => frameList[at],
                },
                parameters);
        },

        // Custom frame generator
        customGenerator(parameters, frameGenerator) {
            if (!Settings.isFunction(frameGenerator)) {
                throw new Error('The provided value is not a frame generator', frameGenerator)
            }
            // Get custom generator returned by the function
            let generator = FrameGenerator(
                frameGenerator(parameters, frameGenerator),
                parameters
            );
            // Wrap up getFrame function so that it returns Frame object instead of raw pixels
            generator.getRawFrame = generator.getFrame;
            generator.getFrame = at => Frame(at, generator.getRawFrame(at), parameters.columns, parameters.rows);
            return generator;
        },


        // Prepares a sequence of frames to play
        createSequence(frames, identifiers, reverse) {
            // Extract a sequence of frames to play
            let sequence;
            if (identifiers && identifiers.length > 0) {
                sequence = identifiers
                    .map(id => frames.find(frame => frame.id == id))
                    .filter(frame => frame != null && !frame.disabled);
            }
            else {
                sequence = Array.from(frames);
            }
            if (reverse) {
                sequence = sequence.reverse();
            }
            return sequence;
        },

        // Evaluates frame transitions for the specified set of frames,
        // by creating an alternative set of frames containing only the modified pixels,
        // to speed up animations.
        createTransitions(frames) {
            // Fixed set of frames is required to pre-calculate transitions.
            // With frame generators we'll be doing this on the fly,
            // as frames cannot be determined beforehand.
            if (frames && frames.canPrerender()) {
                let diffs = [];
                for (let i = 0; i < frames.getCount(); i++) {
                    let frame = frames.getFrame(i);

                    // calculate diffs
                    let previous = frames.getFrame(i - 1);
                    let next = frames.getFrame(i + 1);
                    frame.diff.index = i;
                    frame.diff.forward = previous ? [] : null;
                    frame.diff.reverse = next ? [] : null;
                    for (let y = 0; y < frame.rows; y++) {
                        let row = frame.data[y] || [];
                        let previousRow = previous ? previous.data[y] || [] : null;
                        let nextRow = next ? next.data[y] || [] : null;
                        for (let x = 0; x < frame.columns; x++) {
                            if (previousRow && previousRow[x] != row[x]) {
                                frame.diff.forward.push({
                                    x: x,
                                    y: y,
                                    color: row[x]
                                });
                            }
                            if (nextRow && row[x] != nextRow[x]) {
                                frame.diff.reverse.push({
                                    x: x,
                                    y: y,
                                    color: row[x]
                                });
                            }
                        }
                    }
                    frame.diff.ratio.forward = frame.diff.forward ? frame.diff.forward.length / (frame.rows * frame.columns) : 1;
                    frame.diff.ratio.reverse = frame.diff.reverse ? frame.diff.reverse.length / (frame.rows * frame.columns) : 1;
                    diffs.push(frame.diff);
                }
                Logger.info('Frame transitions prepared', diffs);
                return diffs;
            }
        },
    };

    // Base UI renderer
    function Renderer(renderer, canvas, parameters, palette) {

        let base = {
            // Initialized flag
            initialized: false,
            // Rendering canvas
            canvas: canvas,
            // Parameters
            parameters: parameters,
            // Color palette
            palette: palette || {
                'background': 'transparent',  // Frame background
                'grid': 'silver',             // Color of grid lines. If not specified, grid isn't rendered
                '0': 'transparent',           // Color of turned-on pixels
                '1': 'black'                  // Color of turned-off pixels
            },

            // Returns color value if color specified as hex rather than palette reference.
            // We support the following color formats:
            //  - palette key
            //  - HTML hex, i.e. #ff00ff
            //  - C hex, i.e. 0xff00ff
            isRGB(color) {
                if (color) {
                    if (color[0] == '#') {
                        return color;
                    }
                    else if (color[0] == '0' && color[1] == 'x') {
                        return '#' + color.substr(2);
                    }
                }
            },

            // Sets a color of the specified pixel, optionally sets cell content
            renderPixel(y, x, color, content) {
                throw new Error('Not implemented');
            },

            // Clears the specified pixel
            clearPixel(y, x) {
                throw new Error('Not implemented');
            },

            // Clears the frame
            clearFrame() {
                for (let y = 0; y < this.parameters.rows; y++) {
                    for (let x = 0; x < this.parameters.columns; x++) {
                        base.clearPixel(y, x);
                    }
                }
            },

            // Paints the specified frame
            renderFrame(frameGenerator, frame, allowDiff, reverseDirection) {
                if (frame) {
                    allowDiff = allowDiff && (reverseDirection ? frame.hasReverseDiff() : frame.hasForwardDiff());
                    if (allowDiff) {
                        this.renderDiffFrame(frameGenerator, frame, reverseDirection);
                    }
                    else {
                        this.renderRawFrame(frameGenerator, frame);
                    }
                    return true;
                }
            },

            // Paints the specified frame using raw pixel data
            renderRawFrame(frameGenerator, frame) {
                if (frame && frame.hasData()) {
                    for (let y = 0; y < this.parameters.rows; y++) {
                        let row = frame.data[y];
                        if (row) {
                            for (let x = 0; x < this.parameters.columns; x++) {
                                let pixel = frameGenerator.getPixel(frame.data, x, y);
                                let content = frameGenerator.getPixelContent(frame.data, x, y, pixel);
                                if (pixel !== undefined) {
                                    this.renderPixel(y, x, pixel, content);
                                }
                            }
                        }
                    }
                    Logger.info("FULL frame rendered");
                    return true;
                }
            },

            // Paints the specified frame using diff data.
            renderDiffFrame(frameGenerator, frame, reverseDirection) {
                if (frame) {
                    let pixels = reverseDirection ? frame.diff.reverse : frame.diff.forward;
                    if (pixels) {
                        for (let pixel of pixels) {
                            this.renderPixel(pixel.y, pixel.x, pixel.color);
                        }
                        Logger.info("DIFF frame rendered");
                    }
                    return true;
                }
            }
        };

        if (renderer) {
            renderer.canvas = canvas;
            renderer.parameters = parameters;
            renderer.base = {};
            // Keep overridden methods in the generator intact,
            // inject all the rest of base methods.
            for (let key of Object.keys(base)) {
                if (renderer[key] == undefined) {
                    renderer[key] = base[key];
                }
                else {
                    // Store the base overridden methods in .base property,
                    // just in case the descendant needs them
                    renderer.base[key] = base[key];
                }
            }
        }
        else {
            renderer = base;
        }

        return renderer;
    }

    function DOMRenderer(canvas, parameters, palette) {
        let renderer = {
            // DOM elements representing pixels of the player (two-dimensional array)
            pixels: [],

            // Renders the animation surface
            initialize() {
                renderer.initializeStyles();
                renderer.initializeCanvas();
                renderer.initialized = true;
            },

            // Creates custom stylesheet for the player
            initializeStyles() {
                let palette = renderer.palette;
                let playerName = (renderer.parameters.name || '').trim();
                let sheetId = `ani-mate-player-styles-${renderer.parameters.name}`;
                let playerClass = playerName == '' ? '' : `*[ani-mate-player].${playerName}`;

                // Remove previous styles for this player, if initialized again
                let sheet = document.querySelector(sheetId);
                if (sheet) {
                    sheet.parentElement.removeChild(sheet);
                }
                sheet = document.createElement('style');
                sheet.id = sheetId;

                // Create CSS stylesheet for player elements
                let styles = [];
                if (renderer.parameters.width) {
                    styles.push(`${playerClass} { width: ${renderer.parameters.width}${isNaN(renderer.parameters.width) ? '' : 'px'}; }`);
                }
                if (renderer.parameters.height) {
                    styles.push(`${playerClass} { height: ${renderer.parameters.height}${isNaN(renderer.parameters.height) ? '' : 'px'}; }`);
                }
                if (palette.background) {
                    styles.push(`${playerClass} { background-color: ${palette.background}; }`);
                }
                styles.push(`${playerClass} .row .cell { width: ${100 / renderer.parameters.columns}%; }`);
                if (renderer.parameters.grid && palette.grid) {
                    styles.push(`${playerClass} .row .cell { border: solid ${palette.grid} ${renderer.parameters.grid}; }`);
                }
                if (renderer.parameters.frameSmooth > 0) {
                    styles.push(`${playerClass} .row .cell { transition: all ${renderer.parameters.frameSmooth}ms ease-in; }`);
                }
                for (let key of Object.keys(palette)) {
                    styles.push(`${playerClass} .row .cell.c-${key} { background-color: ${palette[key]}; }`);
                }
                styles.push('');

                sheet.innerHTML = styles.join('\n');
                document.body.appendChild(sheet);
                Logger.info(`Custom styles for player ${renderer.parameters.name} added`);
            },

            // Creates canvas for playing the animations
            initializeCanvas() {
                // Clear
                Logger.info('Creating canvas');
                let playerName = (renderer.parameters.name || '').trim();
                renderer.canvas.classList.remove('visible');
                renderer.canvas.classList.add(playerName);
                renderer.canvas.innerHTML = '';

                // Create cells inside the canvas
                renderer.pixels = [];
                let frameElement = document.createElement('div');
                frameElement.className = 'frame';
                for (let y = 0; y < renderer.parameters.rows; y++) {
                    let rowElement = document.createElement('div');
                    rowElement.className = 'row';
                    rowElement.name = y.toString();
                    let row = [];
                    for (let x = 0; x < renderer.parameters.columns; x++) {
                        let cell = document.createElement('div');
                        cell.className = 'cell';
                        cell.name = x.toString();
                        rowElement.appendChild(cell);
                        row.push(cell);
                    }
                    renderer.pixels.push(row);
                    frameElement.appendChild(rowElement);
                }
                renderer.canvas.appendChild(frameElement);
                Logger.info('Canvas created', renderer.pixels);
            },

            // Sets a color of the specified pixel, optionally sets cell content
            renderPixel(y, x, color, content) {
                if (color != undefined) {
                    let element = renderer.pixels[y][x];
                    if (element) {
                        let rgb = renderer.isRGB(color);
                        let className = rgb ? 'cell' : `cell c-${color}`;
                        let bgColor = rgb ? rgb : '';
                        if (element.className !== className) element.className = className;
                        if (element.style.backgroundColor !== bgColor) element.style.backgroundColor = bgColor;
                        if (content !== undefined && element.innerHTML !== content) {
                            element.innerHTML = content;
                        }
                    }
                }
            },

            // Clears the specified pixel
            clearPixel(y, x) {
                let row = renderer.pixels[y];
                if (row) {
                    let element = row[x];
                    if (element) {
                        element.className = 'cell';
                        element.style.backgroundColor = '';
                        element.style.innerHTML = '';
                    }
                }
            },
        };

        return Renderer(renderer, canvas, parameters, palette);
    }

    function CanvasRenderer(canvas, parameters, palette) {
        let renderer = {

        };
        return Renderer(renderer, canvas, parameters, palette);
    }

    let Renderers = {
        create: (canvas, parameters, palette) => {
            let renderer;
            switch (parameters.renderer || 'dom') {
                case 'dom':
                    renderer = DOMRenderer(canvas, parameters, palette);
                    break;
                case 'canvas':
                    renderer = CanvasRenderer(canvas, parameters, palette);
                    break;
                default:
                    throw new Error(`Unsupported renderer type: ${parameters.renderer}`);
            }
            renderer.initialize();
            return renderer;
        },
    };

    // Play loop
    let PlayLoop = {

        create(iterations) {
            let iteration = 0;
            let count = (iterations === true) ? Number.MAX_VALUE : parseInt(iterations || 1);
            let instance = {
                count: () => count,
                current: () => iteration,
                isInfinite: () => count == Number.MAX_VALUE,
                goForward: () => iteration = Math.min(iteration + 1, count - 1),
                isFirst: () => iteration == 0,
                isLast: () => iteration >= count - 1,
                reset: () => iteration = 0
            };
            return instance;
        },
    };

    // Frame player
    let Player = {
        // Timer executing the sequence
        timer: null,
        // Indicates that the player is currently playing
        isPlaying: false,
        // Indicates that the player has been paused
        isPaused: false,
        // Frames generator
        frames: null,
        transitions: null,
        // Frame renderer
        renderer: null,
        // Loop controller
        loop: null,

        // Initializes the player
        initialize(sequence) {
            // Create the frame provider for the specified sequence, calculate transitions
            Player.frames = FrameGenerators.createGenerator(Settings.parameters, Settings.frames, sequence);
            Player.transitions = FrameGenerators.createTransitions(Player.frames);
            Player.loop = PlayLoop.create(Settings.parameters.loop);
            // Create frame renderer
            Player.renderer = Player.renderer || Renderers.create(Settings.canvas, Settings.parameters, Settings.palette);
            // Start
            Player.reset();
            Logger.info('Player initialized', Settings);
            return true;
        },

        // Notifies that player is ready
        ready() {
            Executor.execute(Settings.events.ready);
        },

        // Resets player state
        reset() {
            Player.isPaused = false;
            Player.isPlaying = false;
            Player.frames.reset();
        },

        // Returns play speed of a current frame
        getCurrentFrameSpeed () {
            return Player.getFrameSpeed(
                Player.frames.getIndex(),
                Player.frames.getCount(),
                Player.frames.isReversed());
        },

        // Returns play speed of the specified frame
        getFrameSpeed(index, count, isReversed) {
            if (Settings.parameters.frameSpeed > 0) {
                return Settings.parameters.frameSpeed;
            }
            else if (typeof Settings.parameters.frameSpeed == 'function') {
                return Settings.parameters.frameSpeed(index, count, isReversed);
            }
            else {
                let frame = Player.frames.getFrame(index);
                if (frame && frame.speed > 0) {
                    return frame.speed;
                }
            }
        },

        // Clears the frame
        clear() {
            return Player.renderer.clearFrame();
        },

        // Reverses play directions
        reverse() {
            if (Player.frames.canReverse()) {
                if (Player.frames.reverse()) {
                    Logger.info('Playback direction reversed');
                    Executor.execute(Settings.events.reverse);
                    return true;
                }
            }
            else {
                Logger.warn('Playback direction cannot be reversed');
            }
        },

        // Skips the specified number of frames or moves forward, if no steps specified
        skip(steps) {
            if (Player.frames.canMove()) {
                Player.frames.skip(steps || 1);
                Logger.info(`Moved to frame #${Player.frames.getIndex()}`);
                Executor.execute(Settings.events.move);
                return true;
            }
            else {
                Logger.warn('The player cannot move arbitrarily to another frame');
            }
        },

        // Returns true if looping is allowed with the current player
        canLoop: () => Player.frames.canLoop(),

        // Returns true if player has reached the last loop
        isLastLoop: () =>  !Player.canLoop() || Player.loop.isLast(),

        // Proceeds with next iteration
        iterate() {
            if (!Player.isLastLoop()) {
                Player.loop.goForward();
                Executor.execute(Settings.events.loop);
            }
        },

        // Moves to the specified frame
        moveTo(index) {
            if (Player.frames.canMove()) {
                Player.frames.goTo(index);
                Logger.info(`Moved to frame #${Player.frames.getIndex()}`);
                Executor.execute(Settings.events.move);
                return true;
            }
            else {
                Logger.warn('The player cannot move arbitrarily to another frame');
            }
        },

        // Paints the current frame
        // If allowDiff is true (by default), differential frame will be used for better rendering speed
        playCurrent(allowDiff, isReversed) {
            allowDiff = (allowDiff === undefined) ? Player.isPlaying : allowDiff;
            isReversed = (isReversed === undefined) ? Player.isPlaying && Player.frames.isReversed() : isReversed;
            let frame = Player.frames.getCurrentFrame();
            if (frame) {
                Player.renderer.renderFrame(Player.frames, frame, allowDiff, isReversed);
                Player.frames.onRender();
                Executor.execute(Settings.events.frame);
                Logger.info(`Frame #${Player.frames.getIndex()} played`, frame);
                return true;
            }
        },

        // Paints a raw frame, outside the sequence
        playRaw(frame) {
            if (Player.frames.canPlayRaw()) {
                frame = Frame('raw', frame, Settings.parameters.columns, Settings.parameters.rows);
                if (Player.renderer.renderRawFrame(Player.frames, frame)) {
                    Executor.execute(Settings.events.frame);
                    Logger.info(`Raw frame rendered`, frame);
                    return true;
                }
            }
            else {
                Logger.warn('The player cannot render arbitrary frames');
            }
        },

        // Paints the specified frame
        playAt(index) {
            if (Player.frames.canMove()) {
                Player.frames.goTo(index);
                Player.playCurrent(false);
                Logger.info(`Frame #${index} played`);
                return true;
            }
            else {
                Logger.warn('The player cannot render frames from arbitrary position');
            }
        },

        // Paints the current frame and proceeds forward
        playNext() {
            if (Player.frames.canMove()) {
                if (!Player.frames.isLast()) {
                    Player.frames.goForward();
                }
                Player.playCurrent();
                return true;
            }
            else {
                Logger.warn('The player cannot arbitrarily move forward');
            }
        },

        // Paints the current frame and proceeds backwards
        playPrevious() {
            if (Player.frames.canMoveBack()) {
                if (!Player.frames.isFirst()) {
                    Player.frames.goBack();
                }
                Player.playCurrent(true, !Player.frames.isReversed());
                return true;
            }
            else {
                Logger.warn('The player cannot arbitrarily move back');
            }
        },

        // Paints the first frame in the current sequence (minding the current direction)
        playFirst() {
            if (Player.frames.canMoveBack()) {
                Player.frames.goFirst();
                Player.frames.onStart();
                Player.playCurrent(false);
                return true;
            }
            else {
                Logger.warn('The player cannot arbitrarily move back');
            }
        },

        // Paints the last frame in the current sequence (minding the current direction)
        playLast() {
            if (Player.frames.canMove()) {
                Player.frames.goLast();
                Player.playCurrent(false);
                return true;
            }
            else {
                Logger.warn('The player cannot arbitrarily move forward');
            }
        },

        // Plays the currently specified sequence of frames
        play() {
            Player.reset();
            Logger.info(`Playing sequence`, Settings.parameters.sequence);
            Logger.info(`Iteration #${Player.loop.current() + 1}/${Player.loop.isInfinite() ? '∞' : Player.loop.count()}`);
            Player.frames.onStart();
            Player.frames.onNext();
            return Player.continue();
        },

        // Resumes playing the current sequence
        continue() {
            if (Player.frames.isEmpty()) {
                Logger.info('Nothing to play, really ...');
                return Promise.resolve();
            }

            Player.isPlaying = true;
            Player.isPaused = false;
            Executor.execute(Settings.events.play);
            return new Promise((resolve, reject) => {

                // Creates a timer which plays the current frame and proceeds with iterations
                function createPlaybackTimer(start) {
                    // Exit if not playing any more
                    if (!Player.isPlaying) {
                        return;
                    }

                    // Stop previous timer if any
                    if (Player.timer) {
                        window.clearTimeout(Player.timer);
                        Player.timer = null;
                    }

                    // Determine playback speed for the current frame
                    let speed;
                    if (start) {
                        speed = 0;
                    }
                    else {
                        speed = start ? 0 : Player.getCurrentFrameSpeed();
                        if (!(speed > 0)) {
                            return reject(`Invalid speed specified for frame # ${Player.frames.getIndex()}`);
                        }
                    }

                    return window.setTimeout(() => {
                        // If run out of frames ...
                        if (Player.frames.isLast()) {
                            // Play the frame
                            Player.playCurrent();
                            // ... check if all iterations have finished
                            if (Player.isLastLoop()) {
                                Player.stop(false);
                                resolve();
                            }
                            else {
                                // If looping continues ...
                                if (Settings.parameters.autoreverse) {
                                    // autoreverse or ...
                                    Player.reverse();
                                    Player.skip();
                                }
                                else {
                                    // ... start over from the first frame
                                    Player.reset();
                                }
                                Player.iterate();
                                Logger.info(`Iteration #${Player.loop.current() + 1}/${Player.loop.isInfinite() ? '∞' : Player.loop.count()}`);
                                Player.isPlaying = true;
                                Player.timer = createPlaybackTimer();
                            }
                        }
                        else {
                            // Play the frame and proceed
                            Player.playCurrent();
                            Player.skip();
                            Player.timer = createPlaybackTimer();
                        }
                    }, speed);
                }

                Player.timer = createPlaybackTimer(true);
            });
        },

        // Pauses the animation
        pause() {
            if (Player.timer) {
                window.clearTimeout(Player.timer);
                Player.timer = null;
                Player.isPaused = true;
                Player.isPlaying = false;
                Executor.execute(Settings.events.pause);
                return true;
            }
        },

        // Resumes the animation
        resume() {
            if (Player.isPaused) {
                Executor.execute(Settings.events.resume);
                return Player.continue();
            }
        },

        // Stops the animation, optionally resets to the original state
        stop(reset) {
            if (Player.timer) {
                window.clearTimeout(Player.timer);
                Player.timer = null;
                Executor.execute(Settings.events.stop);
                Player.frames.onStop();
            }
            if (reset) {
                Player.playFirst();
                Player.reset();
            }
            else {
                Player.isPaused = false;
                Player.isPlaying = false;
            }
        }

    };

    // Component responsible for fetching and preparing configuration settings
    // for the player
    let Configurator = {

        // Initializes the configurator with initial configuration
        initialize(configuration, element) {
            let result = null;
            if (element) {
                configuration = configuration || {};
                // check whether initial configuration is specified as data-configuration parameter?
                let expression = (element.getAttribute('data-configuration') || '').trim();
                if (expression) {
                    try {
                        result = Configurator.assignFromJS(configuration, Executor.evaluate(expression));
                    }
                    catch (error) {
                        result = fail(`Initial configuration expression ${expression} could not be evaluated`, error);
                    };
                }
                else {
                    result = Configurator.assignFromJS(configuration, {});
                }
            }
            else {
                result = fail('Container element is required');
            }
            Logger.initialize(configuration.parameters.debug);
            return result;
        },

        // Collects configuration settings from another JS object
        assignFromJS(configuration, source) {
            if (configuration && source) {
                configuration.parameters = Object.assign(configuration.parameters || {}, source.parameters || {});
                configuration.events = Object.assign(configuration.events || {}, source.events || {});
                configuration.palette = Object.assign(configuration.palette || {}, source.palette || {});
                configuration.frames = source.frames || configuration.frames || {};
                configuration.script = source.script || configuration.script || {};
                Logger.initialize(configuration.parameters.debug);
            }
            return Promise.resolve(configuration);
        },

        // Collects configuration settings from arguments of the specified DOM element
        assignFromDOM(configuration, element) {
            if (!element || !configuration) {
                return Promise.resolve(configuration);
            }

            Logger.info('Parsing DOM parameters from', element);

            // Get allowed parameter names and event names
            let allowedParameters = Object.keys(Settings.parameters);
            let allowedEvents = Object.keys(Settings.events);

            // Assign player name
            let name = (container.getAttribute('ani-mate-player') || '').trim();
            if (name != '') {
                if (!configuration.parameters) {
                    configuration.parameters = {};
                }
                configuration.parameters.name = name;
            }

            // Loop over attributes
            for (let attribute of Array.from(element.attributes)) {
                let keys = attribute.name.split('-');
                let value = (attribute.value || '').trim();
                // if data- attribute, assign to appropriate parameter
                if (keys[0] == 'data' && value != '') {
                    let propertyName = keys
                        .slice(1)
                        .map((key, i) => i == 0 ? key : key[0].toUpperCase() + key.substr(1))
                        .join('');
                    if (allowedParameters.indexOf(propertyName) > -1) {
                        if (!configuration.parameters) {
                            configuration.parameters = {};
                        }
                        if (Executor.isFunctionCall(value)) {
                            configuration.parameters[propertyName] = Executor.unwrap(value);
                        }
                        else {
                            configuration.parameters[propertyName] = value;
                        }
                    }
                }
                // if event- attribute, assign to appropriate event
                else if (keys[0] == 'event' && value != '') {
                    let eventName = keys
                        .slice(1)
                        .map((key, i) => i == 0 ? key : key[0].toUpperCase() + key.substr(1))
                        .join('');
                    if (allowedEvents.indexOf(eventName) > -1) {
                        if (!configuration.events) {
                            configuration.events = {};
                        }
                        configuration.events[eventName] = value;
                    }
                }
            }

            // Assign frames
            let expression = (container.getAttribute('data-frames') || '').trim();
            if (expression) {
                let frames = Executor.evaluate(expression);
                if (frames) {
                    if (Settings.isFrameList(frames)) {
                        configuration.frames = frames;
                    }
                    else if (Settings.isFrameGenerator(frames)) {
                        configuration.frames = Executor.unwrap(expression);
                    }
                    else {
                        return fail('The specified data-frames value is not supported', frames);
                    }
                }
            }

            Logger.initialize(configuration.parameters.debug);
            Logger.info('Parsing DOM parameters finished', configuration);
            return Promise.resolve(configuration);

        },

        // Fetches the script from specified URL
        fetchScript(configuration, element) {
            let result = Promise.resolve(configuration);
            let script = null;
            let selector = null;
            let url = null;
            if (configuration && configuration.parameters) {
                // Get the script from player element?
                if ((script = element.innerHTML.trim()) != '') {
                    configuration.parameters.script = script;
                }
                // Get the script from other DOM element?
                else if (selector = configuration.parameters.scriptElement) {
                    Logger.info(`Fetching script element`, selector);
                    let scriptContainer = document.querySelector(selector);
                    if (scriptContainer) {
                        let tag = scriptContainer.tagName.toLowerCase();
                        if (tag == 'input' || tag == 'textarea') {
                            configuration.script = (container.value || '').trim();
                        }
                        else {
                            configuration.script = container.innerHTML.trim();
                        }
                    }
                    else {
                        result = fail(`Script container ${selector} not found`);
                    }
                }
                // Get the script from URL?
                else if (url = configuration.parameters.scriptUrl) {
                    Logger.info(`Fetching script from`, url);
                    result = fetch(url, { cache: 'no-cache' })
                        .then(response => response.text())
                        .then(text => configuration.parameters.script = text)
                        .then(() => configuration)
                        .catch(error => {
                            return fail(`Error retrieving script from ${url}`, error);
                        });
                }
            }
            return result;
        },

        // Collects rest of settings from script specified in configuration
        assignFromScript(configuration) {
            if (configuration && configuration.parameters.script) {
                let data = ScriptParser.parseScript(configuration.parameters.script);
                Logger.info('Script parsed', data);
                Configurator.assignFromJS(configuration, data);
            }
            return Promise.resolve(configuration);
        },

        // Fetches and parses the frames from the specified URL
        fetchFrames(configuration) {
            let url = null;
            if (configuration && configuration.parameters && (url = configuration.parameters.frameUrl)) {
                return fetch(url, { cache: 'no-cache' })
                    .then(response => response.text())
                    .then(script => {
                        configuration.frames = ScriptParser.parseFrames(
                            configuration.parameters.frameFormat,
                            configuration.parameters.columns,
                            configuration.parameters.rows,
                            script);
                        configuration.parameters.rows = ScriptParser.state.parameters.rows;
                        configuration.parameters.columns = ScriptParser.state.parameters.columns;
                        return configuration;
                    })
                    .catch(error => fail(`Error retrieving frames from ${url}`, error));
            }
            else {
                return Promise.resolve(configuration);
            }
        },

        // Ensures that player has a name and that container element is marked with it properly
        ensurePlayerName(configuration, element) {
            if (configuration && configuration.parameters && element) {
                // Assign default player name if none, make sure it's on the container as well
                if (!configuration.parameters.name) {
                    configuration.parameters.name = 'player';
                }
                if ((container.getAttribute('ani-mate-player') || '') == '') {
                    container.setAttribute('ani-mate-player', configuration.parameters.name);
                }
            }
            return Promise.resolve(configuration);
        },

        // Creates colour palette using pixels with hard-coded colours
        createPalette(configuration) {
            if (configuration && configuration.frames && Settings.isFrameList(configuration.frames)) {
                Logger.info('Populating color palette');
                let palette = ScriptParser.createPalette(configuration.frames);
                if (palette) {
                    configuration.palette = Object.assign(configuration.palette || {}, palette);
                    Logger.info('Color palette populated, pixel remapped', configuration.palette);
                }
            }
            return Promise.resolve(configuration);
        },

        // Validates the configuration
        validate(configuration) {
            if (!configuration)
                return fail("Configuration is required");
            if (!configuration.frames)
                return fail("Frames not specified");
            if (!configuration.parameters)
                return fail("Parameters not specified");
            if (!(configuration.parameters.columns > 0))
                return fail("Invalid number of frame columns");
            if (!(configuration.parameters.rows > 0))
                return fail("Invalid number of frame rows");
            return Promise.resolve(configuration);
        },

    };

    // External API of the component
    let Controller = {
        // Player state
        name: () => Settings.parameters.name,
        isInitialized: () => Settings.isInitialized,
        isPlaying: () => Player.isPlaying,
        isPaused: () => Player.isPaused,
        isFirst: () => Player.frames.isFirst(),
        isLast: () => Player.frames.isLast(),
        getSpeed: () => Player.getCurrentFrameSpeed(),
        index: () => Player.frames.getIndex(),
        count: () => Player.frames.getCount(),
        parameters: name => name ? Settings.parameters[name] : Settings.parameters,
        script: () => Settings.parameters.script,
        configuration: null,

        // Debug mode logger
        logger: () => Logger,

        // Promise triggered when controller has initialized
        ready: null,

        // Configures the controller
        configure: (configuration) => {
            return Configurator
                .initialize(configuration, container)
                .then(configuration => Configurator.assignFromDOM(configuration, container))
                .then(configuration => Configurator.fetchScript(configuration, container))
                .then(configuration => Configurator.assignFromScript(configuration))
                .then(configuration => Configurator.fetchFrames(configuration))
                .then(configuration => Configurator.ensurePlayerName(configuration, container))
                .then(configuration => Configurator.createPalette(configuration))
                .then(configuration => Configurator.validate(configuration))
                .then(configuration => Controller.initialize(configuration, container));
        },

        // Prepares the controller for playing
        initialize(configuration, element) {
            element.innerHTML = '';
            element.classList.add('visible');
            Settings.initialize(
                configuration.parameters,
                configuration.frames,
                configuration.events,
                configuration.palette,
                element);
            Executor.initialize(Controller);
            Logger.initialize(configuration.parameters.debug);
            Logger.info('Initializing player ...', configuration);
            Player.initialize(configuration.sequence);
            Settings.isInitialized = true;
            return Controller;
        },

        // Resets the player using the initial configuration
        reset() {
            Controller.stop();
            return Controller.configure();
        },

        // Reveals the player
        show() {
            let frame = container.querySelector('.frame');
            if (frame) {
                frame.classList.add('visible');
            }
        },

        // Hides the player
        hide() {
            let frame = container.querySelector('.frame');
            if (frame) {
                frame.classList.remove('visible');
            }
        },

        // Plays a sequence of frames.
        // Allows overriding default parameters received during initialization.
        // Allows specifying a subset of frames to play.
        play(parameters, sequence) {
            if (!Controller.isInitialized()) {
                return fail('Player not initialized');
            }

            // Reset the player
            Player.stop();
            // Override the settings with provided ones, just keep the original ones intact
            Settings.store();
            Settings.initialize(parameters);
            Player.initialize(sequence);
            Controller.show();
            return Player.play()
                .then(() => {
                    // Restore original settings
                    Settings.restore();
                    return Player;
                })
                .catch(error => {
                    Player.stop();
                    return fail("An  error has happened while playing", error);
                });
        },

        // Stops playback
        stop() {
            Controller.show();
            Player.stop(true);
            return !Player.isPlaying;
        },

        // Pauses playback
        pause() {
            Controller.show();
            return Player.pause();
        },

        // Resumes playback
        resume() {
            Controller.show();
            return Player.continue();
        },

        // Restarts playback
        restart(parameters, sequence) {
            Controller.stop();
            return Controller.play(parameters, sequence);
        },

        // Clears the display
        clear() {
            Controller.show();
            Player.clear();
        },

        // Plays a frame at the specified index
        playAt(index) {
            Controller.show();
            return Player.playAt(index);
        },

        // Plays an arbitrary frame
        playRaw(frame) {
            Controller.show();
            return Player.playRaw(frame);
        },

        // Skips by the specified number of frames, negative direction also allowed
        skip(steps) {
            Controller.show();
            return Player.skip(steps);
        },

        // Plays the current frame
        playCurrent() {
            Controller.show();
            return Player.playCurrent();
        },

        // Plays the next frame
        playNext() {
            Controller.show();
            return Player.playNext();
        },

        // Plays the previous frame
        playPrevious() {
            Controller.show();
            return Player.playPrevious();
        },

        // Plays the first frame
        playFirst() {
            Controller.show();
            return Player.playFirst();
        },

        // Plays the last frame
        playLast() {
            Controller.show();
            return Player.playLast();
        },

        // Changes playback speed.
        // You can provide the same values as when initializing the player:
        // a fixed speed, an array of speeds per frame or a function returning speed value
        // The API can be used while player is playing, with instant effect.
        setSpeed(speed) {
            if (speed) {
                if (!isNaN(speed)) {
                    if (speed < 0) {
                        throw new Error('Speed cannot be negative');
                    }
                }
                else if (typeof speed != 'function') {
                    throw new Error('Speed has to be a number or a function returning a number');
                }
                Settings.parameters.frameSpeed = speed;
            }
        },

        // Notifies that player is ready
        ready() {
            Player.ready();
        },

        // Starts autoplaying if set to do so in configuration
        autoplay() {
            if (Controller.isInitialized()) {
                let autoplay = Settings.parameters.autoplay;
                if (autoplay === true) {
                    Controller.play()
                        .then(() => Logger.info('Done.'))
                        .catch(error => fail('Player cannot play', error));
                }
                else if (autoplay === 'first') {
                    Controller.playFirst();
                }
                else if (autoplay === 'last') {
                    Controller.playLast();
                }
                return Promise.resolve(Controller);
            }
            else {
                return Promise.resolve('Player not initialized');
            }
        },

        // Attaches an new event listener
        addEventListener(event, callback) {
            if (Object.keys(Settings.events).indexOf(event) != -1) {
                Settings.events[event] = callback;
            }
        }
    };

    // Reports an error
    function fail(message, exception) {
        if (exception) {
            message = message + ': ' + exception.toString() + '\n' + exception.stack;
        }
        Logger.error(message);
        Executor.execute(Settings.events.error, message);
        return Promise.reject(message);
    }

    return Controller
        .configure()
        .then(controller => controller.autoplay())

}

// Initialize players on the page
window.addEventListener('load', () => {
    let players = Array
        .from(document.querySelectorAll('*[ani-mate-player]'))
        .map(container => AniMatePlayer(container));
    Promise
        .all(players)
        .then(players => {
            players.map(player => player.ready());
        })
        .catch(error => console.error(error));
});

}());
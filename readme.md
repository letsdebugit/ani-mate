# ANI-ME Pixel Animation Player

# Hello World example

# Adding the player to a web page

# Configuring the player
## Introduction
## Configuring the player with inline HTML
    inner html or another element,
    data-script-url
    data-frames-url
    data-frame-speed
    data-frame-generator

## Configuring the player with external script
    frame formats, frames from external file (default, csv, json, piskel c)
## Configuring the player with JS code
    data-configuration + ready
    script can be passed as text, url or selector to container element
    player.addEventListener

# Configuration details
## General syntax
    comments
    empty lines
    section names
    diff
## Player dimensions
    units allowed, if not then px assumed
    use % to fill into the container
## Player behaviour
   autoplay
   loop - count, infinite
   autoreverse
   backwards
## Colors
    palette
    RGB
## Custom styles
## Animation frames
    format declaration, supported formats: simple, csv, pixil
    in simple and csv both color formats can be mixed!

# Controlling the player with JavaScript
## Getting the handle of a player
    use ready event
## Lifecycle events
## Starting, pausing and stopping the player
   can override parameters on play
## Configuring the player using code